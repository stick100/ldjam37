﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class PickupParent : MonoBehaviour {

    SteamVR_TrackedObject trackedObj;
    SteamVR_Controller.Device device;
    public GameObject controllerMesh;

    public Transform sphere;

    void Awake () {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
	}

    void Start()
    {
            trackedObj = GetComponent<SteamVR_TrackedObject>();
    }
	
	void FixedUpdate () {
        device = SteamVR_Controller.Input((int)trackedObj.index);

        if (device.GetTouch(SteamVR_Controller.ButtonMask.Trigger))
        {
            //Debug.Log("You are holding 'Touch' on the Trigger");
        }

        if (device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            //Debug.Log("You activated TouchDown on the Trigger");
        }

        if (device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
        {
            //Debug.Log("You activated TouchUp on the Trigger");
        }

        if (device.GetPress(SteamVR_Controller.ButtonMask.Trigger))
        {
            //Debug.Log("You are holding 'Press' on the Trigger");
        }

        if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            //Debug.Log("You activated PressDown on the Trigger");
        }

        if (device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
        {
            //Debug.Log("You activated PressUp on the Trigger");
        }

        if (device.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad))
        {
            //Debug.Log("You activated PressUp on the Touchpad");
            sphere.transform.position = Vector3.zero;
            sphere.GetComponent<Rigidbody>().velocity = Vector3.zero;
            sphere.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (!(col.gameObject.CompareTag("Pickup") || col.gameObject.CompareTag("PickupBlue") || col.gameObject.CompareTag("PickupRed")))
            return;

        device.TriggerHapticPulse(3500);
        //Would be good to give big outline here
    }

    void OnTriggerExit(Collider col)
    {
        if (!(col.gameObject.CompareTag("Pickup") || col.gameObject.CompareTag("PickupBlue") || col.gameObject.CompareTag("PickupRed")))
            return;

        device.TriggerHapticPulse(500);
        //Would be good to get rid of big outline here
    }

    void OnTriggerStay (Collider col)
    {
        if (!(col.gameObject.CompareTag("Pickup") || col.gameObject.CompareTag("PickupBlue") || col.gameObject.CompareTag("PickupRed")))
            return;

        //Debug.Log("You have collided with " + col.name + " and activated OnTriggerStay");
        if (device.GetTouch(SteamVR_Controller.ButtonMask.Trigger))
        {
            //Debug.Log("You have collided with " + col.name + " while holding down Touch");
            col.attachedRigidbody.isKinematic = true;
            col.gameObject.transform.SetParent(gameObject.transform);
            controllerMesh.SetActive(false);
            //var colorToA = controllerMesh.GetComponent<Renderer>().material.color;
            //controllerMesh.GetComponent<Renderer>().material.color = new Color(colorToA.r, colorToA.g, colorToA.b, .1f);
        }
        if (device.GetTouchUp(SteamVR_Controller.ButtonMask.Grip))
        {
            //Debug.Log("You have released Grip while colliding with " + col.name);
            col.gameObject.transform.SetParent(null);
            //col.attachedRigidbody.isKinematic = false;
            controllerMesh.SetActive(true);
            //var colorToA = controllerMesh.GetComponent<Renderer>().material.color;
            //controllerMesh.GetComponent<Renderer>().material.color = new Color(colorToA.r, colorToA.g, colorToA.b, 1);

            //tossObject(col.attachedRigidbody);
        }
    }

    void tossObject(Rigidbody rigidBody)
    {
        Transform origin = trackedObj.origin ? trackedObj.origin : trackedObj.transform.parent;
        //if (origin != null)
        //{
        //    rigidBody.velocity = new Vector3();//origin.TransformVector(device.velocity);
        //    rigidBody.angularVelocity = new Vector3();//origin.TransformVector(device.angularVelocity);
        //} else
        //{
            //rigidBody.velocity = device.velocity;
            //rigidBody.angularVelocity = device.angularVelocity;
            rigidBody.velocity = new Vector3(0,0);
            rigidBody.angularVelocity = new Vector3(0,0);
        //}
        
    }
}
