﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class DieIfTouched : MonoBehaviour
{

    public string TagThatKills = "BulletOrEnemy";
    public GameManager _gameManager;

	// Use this for initialization
	void Start ()
	{
	    _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        
    }

    void OnTriggerEnter(Collider col)
    {
        if (!(col.gameObject.CompareTag(TagThatKills)))
            return;

        //gameObject.GetComponent<Renderer>().enabled = false;
        //Destroy(gameObject);
        _gameManager.sendEndSignal = true;
        //Sound trigger which one got hit, do that later I guess.
    }
}
