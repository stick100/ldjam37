﻿using System.Collections;
using System.Collections.Generic;
using ProBuilder2.Common;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{

    public GameObject bullet;
    public int doOnceInXTime = 100;
    private int countup = 0;
    

    void Awake()
    {
        //bullet = GameObject.FindGameObjectWithTag("Bullet");
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        countup++;
        if (countup%doOnceInXTime == 0)
        {
            var bulletSent = GameObject.Instantiate(bullet, this.transform.localPosition, this.transform.localRotation);//Todo set common parent to keep heirarchy clean.
            GameObject.Destroy(bulletSent, 8);
        }
    }
}
