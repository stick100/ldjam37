﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

    public float rotSpeed = 60;
    public int doOnceInXTime = 100;
    private int countup = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	void FixedUpdate () {
        countup++;
        if (countup % doOnceInXTime == 0)
            this.transform.Rotate(new Vector3(rotSpeed / Time.deltaTime / 360, 0));
	}
}
