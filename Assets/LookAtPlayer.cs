﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{

    private Transform player;
    public string tagToLookAt = "Pickup";

    void Awake()
    {
        if (GameObject.FindGameObjectsWithTag(tagToLookAt).Length == 0)
        {
            GameObject.Destroy(gameObject);
        }
        player = GameObject.FindGameObjectsWithTag(tagToLookAt).First().transform;//TODO make random so will shot at multiple
    }

	// Use this for initialization
	void Start () {
		
	}

    public float rotSpeed = 60;
    public int doOnceInXTime = 100;
    private int countup = 0;


    void FixedUpdate()
    {
        //countup++;
        if (countup%doOnceInXTime == 0)
        {
            if (player != null)
            {
                var rotation = Quaternion.LookRotation(player.position - transform.position);
                //transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 100);

                //player = Quaternion.LookRotation(player.position - transform.position);
                // Will assume you mean to divide by damping meanings it will take damping seconds to face target assuming it doesn't move
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation,  Time.fixedDeltaTime* rotSpeed/60);
            }
            else
            {
                GameObject.Destroy(gameObject);
                player = GameObject.FindGameObjectsWithTag(tagToLookAt).First().transform;//TODO make random so will shot at multiple
            }
            //this.transform.LookAt(player);
        }
    }

}
