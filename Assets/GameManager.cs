﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Comparers;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Valve.VR;

public class GameManager : MonoBehaviour
{

    public GameObject playerRed;
    public GameObject playerBlue;

    public SpawnEnemies SpawnRed;
    public SpawnEnemies SpawnBlue;

    private Transform starTransformRed;
    private Transform starTransformBlue;

    private float startTime;
    private float endTime;
    private bool stated = false;
    private bool ended = false;

    public Rigidbody RedRigidbody;
    public Rigidbody BlueRigidbody;

    public Text FrontText;
    public Text BackText;

    public float spawnSpeedRed;
    public float spawnSpeedBlue;

    public bool sendEndSignal = false;

    // Use this for initialization
    void Start ()
    {
        starTransformRed = playerRed.transform;
        starTransformBlue = playerBlue.transform;
    }
	
	// Update is called once per frame
	void Update () {
	    if (stated == false && RedRigidbody.isKinematic &&
            BlueRigidbody.isKinematic)
	    {
	        stated = true;
            startTime = Time.time;
	        spawnSpeedRed = SpawnRed.timeToSpawn;
            spawnSpeedBlue = SpawnBlue.timeToSpawn;

            SpawnRed.MaxSpawnCount++;
            SpawnBlue.MaxSpawnCount++;
	        FrontText.text = "";
            BackText.text = "";
        }
	    if (sendEndSignal && !ended)//Either player dies end game
	    {
	        sendEndSignal = false;
            ended = true;

            Time.timeScale = .01f;//Pause Everything
            //Game Over
	        endTime = Time.time - startTime;

            StartCoroutine(Example());
            FrontText.text = "Lasted " + endTime.ToString("F2") + " level will restart in 10 seconds";
            BackText.text = "Lasted " + endTime.ToString("F2") + " level will restart in 10 seconds";
            //Display


            
        }
	}

    public int doOnceInXTime = 10000;
    private int countup = 0;

    void FixedUpdate()
    {
        countup++;
        if (stated && countup % doOnceInXTime == 0)
        {
            SpawnRed.MaxSpawnCount++;
            SpawnBlue.MaxSpawnCount++;
            SpawnRed.timeToSpawn = SpawnRed.timeToSpawn*.9f;
            SpawnBlue.timeToSpawn = SpawnBlue.timeToSpawn * .9f;
        }
    }


    IEnumerator Example()
    {
        yield return new WaitForSeconds(.1f);

        var BlueEnemies = GameObject.FindGameObjectsWithTag("EnemyBlue");
        var RedEnemies = GameObject.FindGameObjectsWithTag("EnemyRed");
        foreach (var VARIABLE in BlueEnemies)
        {
            Destroy(VARIABLE);
        }
        foreach (var VARIABLE in RedEnemies)
        {
            Destroy(VARIABLE);
        }

        //Delete all enemies reset spawns to 0;
        SpawnRed.MaxSpawnCount = 0;
        SpawnBlue.MaxSpawnCount = 0;
        SpawnRed.timeToSpawn = spawnSpeedRed;
        SpawnBlue.timeToSpawn = spawnSpeedBlue;

        //SceneManager.LoadScene(0);
        Time.timeScale = 1;

        stated = true;
        startTime = Time.time;
        spawnSpeedRed = SpawnRed.timeToSpawn;
        spawnSpeedBlue = SpawnBlue.timeToSpawn;

        SpawnRed.MaxSpawnCount++;
        SpawnBlue.MaxSpawnCount++;
        FrontText.text = "";
        BackText.text = "";

        ended = false;

    }

}
