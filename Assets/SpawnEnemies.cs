﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using Boo.Lang;

public class SpawnEnemies : MonoBehaviour {

    public GameObject toSpawn;
    public float timeToSpawn;
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    public float minZ;
    public float maxZ;
    private float timeSinceLastSpawn = 0;
    public GameObject setParent;
    private List<GameObject> enemiesSpawned = new List<GameObject>();
    private List<int> enemiesSweep = new List<int>();
    public int MaxSpawnCount = 10;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        timeSinceLastSpawn += Time.deltaTime;
        if(timeSinceLastSpawn >= timeToSpawn)
        {
            enemiesSweep = new List<int>();
            for (int i = enemiesSpawned.Count-1; i>0; i--)//Add backwards so can remove without messing with index
            {
                if (enemiesSpawned[i] == null || !enemiesSpawned[i].activeInHierarchy)
                {
                    enemiesSweep.Add(i);
                }
            }
            foreach (var indexToRemove in enemiesSweep)
            {
                enemiesSpawned.RemoveAt(indexToRemove);
            }

            //Spawn
            //for(int i = 0; i<10; i++)
            if (enemiesSpawned.Count < MaxSpawnCount)
            {
                var enemy = GameObject.Instantiate(toSpawn,
                    new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), Random.Range(minZ, maxZ)),
                    new Quaternion()); //Need pooling
                //((GameObject)enemy).transform.SetParent(setParent.transform);

                enemiesSpawned.Add(enemy);
            }
            timeSinceLastSpawn = 0;
        }
	}
}
